import React from "react";
import {useQuery} from "react-query";

import ItemRow from "components/AppListRow";
import {getAppInfo} from "./api/AppInfo";
import RecommendBar from "components/RecommendBar";
import Variants from "components/Variants";
import type {EntryItem} from "./interface/IAppInfo";
import "./App.scss";

function scatterArray<T>(appList?: T[]): T[] {
    return [...(appList ?? [])].sort(() => (Math.random() > 0.5 ? 1 : -1));
}

export default function App() {
    const {isLoading, data} = useQuery("", getAppInfo);
    const appList = data as EntryItem[] | undefined;
    return (
        <div className="app-container">
            <div className="main-container">
                {isLoading && <Variants />}
                {!isLoading && (
                    <div>
                        <div className="recommend-bar-little">Recommend</div>
                        <div className="recommend-bar">
                            {scatterArray(appList).map((appItem, index) => (
                                <RecommendBar app={appItem} key={index} />
                            ))}
                        </div>
                        <div className="app-list">
                            {appList?.map((appItem, index) => (
                                <ItemRow app={appItem} index={index + 1} key={index} />
                            ))}
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
}
