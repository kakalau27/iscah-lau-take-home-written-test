import React from "react";

import {EntryItem} from "../../interface/IAppInfo";
import "./appListRow.scss";

type ItemRowProps = {
    app: EntryItem;
    index: number;
};

export default function itemRow({app, index}: ItemRowProps) {
    const isEven = index % 2 ? "app-image-circular" : "app-image-rounded";
    function openAppLink() {
        window.open(app.link[0].attributes.href, "_blank");
    }
    return (
        <div className="row-box" onClick={openAppLink}>
            <p className="ranking-number">{index}</p>
            {<img className={`app-image ${isEven}`} src={app["im:image"][2]["label"]} />}
            <div className="app-info-name-category">
                <span className="app-name">{app["im:name"]["label"]}</span>
                <span className="app-category">{app.category.attributes?.label ?? ""}</span>
            </div>
        </div>
    );
}
