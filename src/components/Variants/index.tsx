import * as React from "react";
import Skeleton from "@mui/material/Skeleton";
import Stack from "@mui/material/Stack";
import "./variants.scss";

export default function Index() {
    return (
        <Stack spacing={1} className="variants-container">
            {/* For variant="text", adjust the height via font-size */}
            <Skeleton variant="text" sx={{fontSize: "1rem"}} />
            <div className="recommend-bar">
                <div className="recommend-bar-app">
                    <Skeleton
                        className="recommend-app-icon"
                        variant="rounded"
                        width={80}
                        height={80}
                    />
                    <Skeleton variant="text" sx={{fontSize: "1px"}} />
                    <Skeleton variant="text" sx={{fontSize: "1px"}} />
                </div>
                <div className="recommend-bar-app">
                    <Skeleton
                        className="recommend-app-icon"
                        variant="rounded"
                        width={80}
                        height={80}
                    />
                    <Skeleton variant="text" sx={{fontSize: "1px"}} />
                    <Skeleton variant="text" sx={{fontSize: "1px"}} />
                </div>
                <div className="recommend-bar-app">
                    <Skeleton
                        className="recommend-app-icon"
                        variant="rounded"
                        width={80}
                        height={80}
                    />
                    <Skeleton variant="text" sx={{fontSize: "1px"}} />
                    <Skeleton variant="text" sx={{fontSize: "1px"}} />
                </div>
                <div className="recommend-bar-app">
                    <Skeleton
                        className="recommend-app-icon"
                        variant="rounded"
                        width={80}
                        height={80}
                    />
                    <Skeleton variant="text" sx={{fontSize: "1px"}} />
                    <Skeleton variant="text" sx={{fontSize: "1px"}} />
                </div>
            </div>
            <div className="app-list-container">
                <Skeleton className="app-list-app-icon" variant="rounded" width={70} height={70} />
                <div className="app-info">
                    <Skeleton variant="text" sx={{fontSize: "1px"}} width={100} />
                    <Skeleton variant="text" sx={{fontSize: "1px"}} width={100} />
                </div>
            </div>
            <div className="app-list-container">
                <Skeleton className="app-list-app-icon" variant="rounded" width={70} height={70} />
                <div className="app-info">
                    <Skeleton variant="text" sx={{fontSize: "1px"}} width={100} />
                    <Skeleton variant="text" sx={{fontSize: "1px"}} width={100} />
                </div>
            </div>
            <div className="app-list-container">
                <Skeleton className="app-list-app-icon" variant="rounded" width={70} height={70} />
                <div className="app-info">
                    <Skeleton variant="text" sx={{fontSize: "1px"}} width={100} />
                    <Skeleton variant="text" sx={{fontSize: "1px"}} width={100} />
                </div>
            </div>
            <div className="app-list-container">
                <Skeleton className="app-list-app-icon" variant="rounded" width={70} height={70} />
                <div className="app-info">
                    <Skeleton variant="text" sx={{fontSize: "1px"}} width={100} />
                    <Skeleton variant="text" sx={{fontSize: "1px"}} width={100} />
                </div>
            </div>
            <div className="app-list-container">
                <Skeleton className="app-list-app-icon" variant="rounded" width={70} height={70} />
                <div className="app-info">
                    <Skeleton variant="text" sx={{fontSize: "1px"}} width={100} />
                    <Skeleton variant="text" sx={{fontSize: "1px"}} width={100} />
                </div>
            </div>
        </Stack>
    );
}
