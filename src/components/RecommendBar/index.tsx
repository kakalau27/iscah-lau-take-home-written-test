import React from "react";

import "./recommendBar.scss";
import {EntryItem} from "../../interface/IAppInfo";

type RecommendBarProps = {
    app: EntryItem;
};

export default function RecommendBar({app}: RecommendBarProps) {
    function openAppLink() {
        window.open(app.link[0].attributes.href, "_blank");
    }
    return (
        <div className="recommend-container" onClick={openAppLink}>
            <div className="app-info">
                <img className="app-image" src={app["im:image"][2]["label"]} />
                <div className="app-info-name-category">
                    <span className="app-name">{app["im:name"]["label"]}</span>
                    <span className="app-category">{app.category.attributes?.label ?? ""}</span>
                </div>
            </div>
        </div>
    );
}
