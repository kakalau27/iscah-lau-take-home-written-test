import axios from "axios";
import {API_URL, APP_COUNT_LIMIT} from "../utils/config";

export const getAppInfo = async () => {
    const response = await axios.get(
        `${API_URL}/hk/rss/topfreeapplications/limit=${APP_COUNT_LIMIT}/json?`
    );
    return response.data?.feed?.entry;
};
