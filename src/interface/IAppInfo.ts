export interface summaryItem {
    label: string;
}

export interface imArtistItem {
    attributes: AttributesItem;
    label: string;
}

export interface AuthorItem {
    name: NameItem;
    uri: UriItem;
}

export interface ImPriceItem {
    attributes: AttributesItem;
    label: string;
}

export interface LinkItem {
    attributes: AttributesItem;
}

export interface IconItem {
    label: string;
}

export interface TitleItem {
    label: string;
}

export interface UriItem {
    label: string;
}

export interface ImReleaseDateItem {
    attributes: AttributesItem;
    label: string;
}

export interface EntryItem {
    summary: summaryItem;
    "im:artist": imArtistItem;
    "im:name": ImNameItem;
    "im:contentType": ImContentTypeItem;
    "im:image": Array<ImImageItem>;
    rights: RightsItem;
    "im:price": ImPriceItem;
    link: Array<LinkItem>;
    id: IdItem;
    title: TitleItem;
    category: CategoryItem;
    "im:releaseDate": ImReleaseDateItem;
}

export interface FeedItem {
    entry: Array<EntryItem>;
    author: AuthorItem;
    rights: RightsItem;
    icon: IconItem;
    link: Array<LinkItem>;
    id: IdItem;
    title: TitleItem;
    updated: UpdatedItem;
}

export interface ImNameItem {
    label: string;
}

export interface ImContentTypeItem {
    attributes: AttributesItem;
}

export interface ImImageItem {
    attributes: AttributesItem;
    label: string;
}

export interface RightsItem {
    label: string;
}

export interface NameItem {
    label: string;
}

export interface AttributesItem {
    rel: string;
    href: string;
    type: string;
    label?: string;
}

export interface IdItem {
    label: string;
}

export interface CategoryItem {
    attributes: AttributesItem;
}

export interface UpdatedItem {
    label: string;
}

export interface RootType {
    feed: Array<FeedItem>;
}
