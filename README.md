## About The Project

### Project Name

iscah-lau-take-home-written-test

### Built With
* React.js
* Material-UI

### Responsive
* Mobile
* Desktop

## Getting Started
Clone down this repository. You will need `node` and `yarn` installed globally on your machine.

#### Installation:

`npm install --global yarn`

`yarn install`

#### To Start Server:

`yarn run start`

#### To Visit App:

`localhost:8080/`
